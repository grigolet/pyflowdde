pyflowdde
=========

.. image:: https://img.shields.io/pypi/v/pyflowdde.svg
    :target: https://pypi.python.org/pypi/pyflowdde
    :alt: Latest PyPI version

.. image:: None.png
   :target: None
   :alt: Latest Travis CI build status

A high level API to interact with Bronkhorst's FlowDDE

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`pyflowdde` was written by `Gianluca Rigoletti <gianluca.rigoletti@cern.ch>`_.
